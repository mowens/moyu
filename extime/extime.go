package extime

import (
	"fmt"
	"log"
	"time"
)

var IsLog bool

func Tominute() int64 {
	t := time.Now()
	newTime := time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), 0, 0, t.Location())
	if IsLog {
		log.Printf("moyu/extime.Today() today=%d", newTime.Unix())
	}
	return newTime.Unix()
}
func Tohour() int64 {
	t := time.Now()
	newTime := time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), 0, 0, 0, t.Location())
	if IsLog {
		log.Printf("moyu/extime.Today() today=%d", newTime.Unix())
	}
	return newTime.Unix()
}
func Today() int64 {
	t := time.Now()
	newTime := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
	if IsLog {
		log.Printf("moyu/extime.Today() today=%d", newTime.Unix())
	}
	return newTime.Unix()
}
func Tomonth() int64 {
	t := time.Now()
	newTime := time.Date(t.Year(), t.Month(), 0, 0, 0, 0, 0, t.Location())
	if IsLog {
		log.Printf("moyu/extime.Today() today=%d", newTime.Unix())
	}
	return newTime.Unix()
}
func Toyear() int64 {
	t := time.Now()
	newTime := time.Date(t.Year(), 0, 0, 0, 0, 0, 0, t.Location())
	if IsLog {
		log.Printf("moyu/extime.ToYear() today=%d", newTime.Unix())
	}
	return newTime.Unix()
}

//格式化
func ToDate() string {
	return ToDatef("-")
}

func ToDatef(cut string) string {
	now := time.Now()
	return fmt.Sprintf("%d%s%d%s%d", now.Year(), cut, now.Month(), cut, now.Day())
}
