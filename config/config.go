package config

import (
	"fmt"
	"log"
	"os"
)

var IsLog bool
var Configs = make(map[string]interface{})
var ConfN = make(map[string]map[string]string)
var Conf = make(map[string]string)

func ReadConfig(path string) map[string]interface{} {
	f, err := os.Open(path)
	if err != nil {
		log.Printf("打开文件错误：%s \n", err.Error())
	}
	conf := make(map[string]interface{})
	confTemp := make(map[string]string)
	var readsLine, group = true, false
	var cut, index int
	cache := make([]byte, 1)
	var tempStr, groupKey string
	for {
		//读取一个字节
		_, err := f.Read(cache)
		if err != nil {
			break
		}
		//过滤注释字符
		if string(cache) == "#" {
			readsLine = false
			continue
		}
		//处理分组符
		if string(cache) == "[" {
			group = true
			if len(confTemp) > 0 {
				conf[groupKey] = mapCopy(confTemp)
				mapClear(confTemp)
			}
			continue
		}
		//分组终止
		if string(cache) == "]" {
			groupKey = tempStr
			continue
		}

		//当行被注释且不为换行符时，无需往下处理
		if !readsLine && (string(cache) != "\n" || string(cache) != "\n") {
			continue
		}
		if string(cache) == "=" || string(cache) == ":" {
			cut = index
			continue
		}
		//读取换行符
		if string(cache) == "\n" || string(cache) == "\n" {
			readsLine = true
			index = 0
			if cut == 0 {
				tempStr = ""
				continue
			}
			if group {
				//分组隔离
				if cut == len(tempStr) {
					fmt.Printf("读取错误：错误长度%d,字符长度%d,错误=>%s \n", cut, len(tempStr), tempStr)
				} else {
					confTemp[tempStr[0:cut]] = tempStr[cut : len(tempStr)-1]

				}
			} else {
				conf[tempStr[0:cut]] = tempStr[cut : len(tempStr)-1]
			}
			cut = 0
			tempStr = ""
			continue
		}
		index += 1
		tempStr += string(cache)
	}
	//添加最后读取的数据
	if group {
		conf[groupKey] = mapCopy(confTemp)
		mapClear(confTemp)
	}
	//打印日志
	if IsLog {
		for k, v := range conf {
			switch v.(type) {
			case string:
				fmt.Printf("%s=%+v \n", k, v)
			case map[string]string:
				fmt.Printf("gourp:%s \n", k)
				for k2, v2 := range v.(map[string]string) {
					fmt.Printf("    %s=%+v \n", k2, v2)
				}
			}
		}
	}
	return conf
}
func mapCopy(m map[string]string) map[string]string {
	nm := make(map[string]string)
	for k, v := range m {
		nm[k] = v
	}
	return nm
}
func mapClear(m map[string]string) {
	for k, _ := range m {
		delete(m, k)
	}
}
func Format(m map[string]interface{}) {
	for k, v := range m {
		switch v.(type) {
		case string:
			Conf[k] = v.(string)
		case map[string]string:
			ConfN[k] = make(map[string]string)
			for k2, v2 := range v.(map[string]string) {
				ConfN[k][k2] = v2
			}
		}
	}
	if IsLog {
		fmt.Printf("Conf:%+v \n", Conf)
		fmt.Printf("ConfN:%+v \n", ConfN)
	}
}
