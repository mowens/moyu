module gitee.com/mowens/moyu

go 1.16

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jinzhu/gorm v1.9.16
	github.com/onsi/gomega v1.12.0 // indirect
)
