package main

import (
	"bytes"
	"io/ioutil"
	"log"

	"gitee.com/mowens/moyu/exhttp"
)

func main() {
	eh := exhttp.NewRequests()
	eh.ClientCert = "cert/apiclient_cert.pem"
	eh.ClientKey = "cert/apiclient_key.pem"
	body := "token=8d2c5bddea6d53222f763bc0c1362b78"
	resp, err := eh.Post("https://farm.joybytech.com/test/hbt", "form", bytes.NewBuffer([]byte(body)))
	if err != nil {
		log.Printf("请求错误：%s\n", err.Error())
	}
	defer resp.Body.Close()
	bodystr, _ := ioutil.ReadAll(resp.Body)
	log.Println("response Body:", string(bodystr))
}
