package mongo

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var Cmongo *mongo.Client
var MongoUrl string

func NewMongo() *mongo.Client {
	clientOptions := options.Client().ApplyURI(MongoUrl)
	client, err := mongo.NewClient(clientOptions)
	//client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Printf("建立新客户端错误" + MongoUrl)
		log.Fatal(err)
	}

	if err = client.Connect(context.TODO()); err != nil {
		log.Printf("连接MongoDB客户端错误:")
		log.Fatal(err)
	}

	// 检查连接情况
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Printf("Ping MongoDB ")
		log.Fatal(err)
	}
	log.Println("成功连接到MongoDB!")
	return client
}

func MClose() {
	Cmongo.Disconnect(context.TODO())
}
