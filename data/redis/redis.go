package redis

import (
	"context"

	"github.com/go-redis/redis"
)

var Ctx = context.Background()
var RDB *redis.Client

func InitRedis(addr, pwd string, db int) *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     addr, // "localhost:6379",
		Password: pwd,  // no password set
		DB:       db,   // use default DB
	})
}
