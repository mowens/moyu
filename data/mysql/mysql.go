package mysql

import (
	"fmt"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var DB *gorm.DB

type MysqlConf struct {
	User         string
	Password     string
	Host         string
	Port         string
	Database     string
	Charset      string
	MaxIdleConns int
	MaxOpenConns int
}

func (m *MysqlConf) InitDb() *gorm.DB {
	sqlConn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=True&loc=Local", m.User, m.Password, m.Host, m.Port, m.Database, m.Charset)
	db, err := gorm.Open("mysql", sqlConn)
	if err != nil {
		log.Panic(err)
		os.Exit(-1)
	}
	//db.SingularTable(true)                  //全局设置表名不可以为复数形式。
	db.DB().SetMaxIdleConns(m.MaxIdleConns) //空闲时最大的连接数
	db.DB().SetMaxOpenConns(m.MaxOpenConns) //最大的连接数
	log.Println("成功连接数据库")
	return db
}
