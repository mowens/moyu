package exhttp

import (
	"crypto/tls"
	"io"
	"log"
	"net/http"
)

//返回值
type response struct {
	Code int64       `json:"code"` //响应状态
	Msg  string      `json:"msg"`  //响应描述
	Data interface{} `json:"data"` //响应内容
}

func NewResponse() *response {
	return &response{}
}
func (r *response) SuccessResponse(data interface{}) *response {
	r.Code = 1
	r.Msg = "请求成功"
	r.Data = data
	return r
}
func (r *response) ErrorResponse(Code int64, errMsg string) *response {
	r.Code = Code
	r.Msg = errMsg
	r.Data = map[string]interface{}{}
	return r
}

type requests struct {
	ClientCert string //客户端证书
	ClientKey  string //客户端密钥
}

func NewRequests() *requests { return &requests{} }

//带证书的post请求
func (r *requests) Post(url string, contentType string, body io.Reader) (*http.Response, error) {
	var tr *http.Transport
	// 微信提供的API证书,证书和证书密钥 .pem格式
	certs, err := tls.LoadX509KeyPair(r.ClientCert, r.ClientKey)
	if err != nil {
		log.Println("证书加载错误:", err)
		return &http.Response{}, err
	} else {
		tr = &http.Transport{
			TLSClientConfig: &tls.Config{
				//RootCAs:      pool,
				Certificates: []tls.Certificate{certs},
			},
		}
	}
	client := &http.Client{Transport: tr}
	return client.Post(url, contentType, body)
}
