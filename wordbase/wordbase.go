package wordbase

import (
	"math/rand"
	"strconv"
	"time"
)

const (
	str_first            = 32  //字符起始值 32为ASCII码中的空格
	str_len              = 95  //字符结束值 127为空格（看起来像） 超出127的为特殊字符不可用
	str_small_first      = 97  //小写字符起始值
	str_small_end        = 122 //小写字符起始值
	capital_letter_first = 65  //大写字母起始值
	capital_letter_end   = 90  //大写字母结束值
	number_first         = 48  //数值型起始值
	number_end           = 57  //数值型结束值
)

var r = rand.New(rand.NewSource(time.Now().Unix()))

//小写字符
func RandSmall(leng int) string {
	bytes := make([]byte, leng)
	for i := 0; i < leng; i++ {
		n := r.Intn(str_small_end+1-str_small_first) + str_small_first
		bytes[i] = byte(n)
	}
	return string(bytes)
}

//大写字符
func RandCapital(leng int) string {
	bytes := make([]byte, leng)
	for i := 0; i < leng; i++ {
		n := r.Intn(capital_letter_end+1-capital_letter_first) + capital_letter_first
		bytes[i] = byte(n)
	}
	return string(bytes)
}

//随机整数值
//返回字符串
func RandNumber(leng int) string {
	bytes := make([]byte, leng)
	for i := 0; i < leng; i++ {
		n := r.Intn(number_end+1-number_first) + number_first
		bytes[i] = byte(n)
	}
	return string(bytes)
}

//随机整数值
//返回int
func RandNumberToInt(leng int) int {
	bytes := make([]byte, leng)
	for i := 0; i < leng; i++ {
		n := r.Intn(number_end+1-number_first) + number_first
		bytes[i] = byte(n)
	}
	temp := string(bytes)
	number, err := strconv.Atoi(temp)
	if err == nil {
		return number
	}
	return 0
}

//不区分大小写
func RandSmallCapital(leng int) string {
	bytes := make([]byte, leng)
	for i := 0; i < leng; i++ {
		n := r.Intn(str_small_end + 1 - capital_letter_first)
		n = n + capital_letter_first
		//跳过特殊符号
		if n > capital_letter_end && n < str_small_first {
			i = i - 1
			continue
		}
		bytes[i] = byte(n)
	}
	return string(bytes)
}

//不区分大小写与特殊符号
func RandString(leng int) string {
	bytes := make([]byte, leng)
	for i := 0; i < leng; i++ {
		n := r.Intn(str_len) + str_first
		bytes[i] = byte(n)
	}
	return string(bytes)
}

//小写转大写
func MallToCap(str string) string {
	var temp string
	for _, v := range str {
		if v >= str_small_first && v <= str_small_end {
			temp += string(v - 32)
		} else {
			temp += string(v)
		}
	}
	return temp
}
