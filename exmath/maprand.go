package exmath

import (
	"time"
)

//自定义随机取值，按设计概率取值
//千次以上便已经趋于预设比例
//需预先计算好比例，不支持同比扩展
func RatiosRandomFree(m map[string]int64) string {
	var sumN int64
	//fmt.Printf("参数M：%v \n", m)
	for _, v := range m {
		sumN += v
	}
	lucky := RandomN(sumN, 1, time.Now().Unix())
	//划段随机发
	//random N = 43			N>45<=99			N>99<=258			N>258<=888			N>88<=999
	// min 0===========45=================99====================258==================888=======>999 max
	//端长即段值	45				54				159						630				111
	var tempVal int64
	var accident string
	for k, v := range m {
		//v+tempVal = 当前段+前N端叠加长度
		//命中值 需大于前N端即大于 tempVal
		//命中值 需小于等于当前段+前N段
		if lucky > tempVal && lucky <= v+tempVal {
			return k
		}
		tempVal += v
		accident = k
	}
	return accident

}
func RatiosRandomFreeInt(m map[int]int64) int {
	var sumN int64
	//fmt.Printf("参数M：%v \n", m)
	for _, v := range m {
		sumN += v
	}
	lucky := RandomN(sumN, 1, time.Now().Unix())
	//划段随机发
	//random N = 43			N>45<=99			N>99<=258			N>258<=888			N>88<=999
	// min 0===========45=================99====================258==================888=======>999 max
	//端长即段值	45				54				159						630				111
	var tempVal int64
	for k, v := range m {
		//v+tempVal = 当前段+前N端叠加长度
		//命中值 需大于前N端即大于 tempVal
		//命中值 需小于等于当前段+前N段
		if lucky > tempVal && lucky <= v+tempVal {
			return k
		}
		tempVal += v
	}
	return 0

}
func RatiosRandomFreeInt64(m map[int64]int64) int64 {
	var sumN int64
	//fmt.Printf("参数M：%v \n", m)
	for _, v := range m {
		sumN += v
	}
	lucky := RandomN(sumN, 1, time.Now().Unix())
	//划段随机发
	//random N = 43			N>45<=99			N>99<=258			N>258<=888			N>88<=999
	// min 0===========45=================99====================258==================888=======>999 max
	//端长即段值	45				54				159						630				111
	var tempVal int64
	for k, v := range m {
		//v+tempVal = 当前段+前N端叠加长度
		//命中值 需大于前N端即大于 tempVal
		//命中值 需小于等于当前段+前N段
		if lucky > tempVal && lucky <= v+tempVal {
			return k
		}
		tempVal += v
	}
	return int64(0)

}
