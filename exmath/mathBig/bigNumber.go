package mathBig

import (
	_ "log"
	"math/big"
)

//计算字符串数值的n次幂
//不支持浮点数计算 浮点数将去掉小数点计算
//a string 计算的目标数值字符串
//floats int 目标字符串小数部分长度
//b int 次幂n
// scale 要保留的目标有效小数
func MPow(a string, floats int, b int, scale int) string {
	number := mPow(a, b)
	sca := floats * b
	//log.Println("小数长度：", sca)
	return numberToFloat(number, sca, scale)
}
func mPow(a string, b int) string {
	if b == 0 {
		return "1"
	}
	if b == 1 {
		return a
	}

	n := new(big.Int)
	m := new(big.Int)
	m.SetInt64(1)
	n, ok := n.SetString(a, 10)
	if !ok {
		return ""
	}
	//可使用channel进行并发提速
	for {
		m.Mul(m, n)
		b = b - 1
		if b == 0 {
			break
		}
	}
	return m.String()
}

//将计算好的字符串数值 转为目标浮点型
//number 目标字符串
//sca 小数部分
//scale 小数部分最大精确位数 无小数则为0.0
func numberToFloat(number string, sca, scale int) string {
	if sca == 0 && scale == 0 {
		return number
	}
	len_number := len(number)
	//log.Println("长度：", len_number)
	//log.Println("原值：", number)
	if len_number > scale {
		//log.Println("完整长度：", len(number))1000112 10001.12 2 32 7-0=5
		//完整长度-小数长度+预设小数部分=截取目标长度
		if sca <= scale {
			scale = sca
		}
		number = number[:len_number-sca+scale]
	}
	//log.Println("整数部分", number[:len_number-sca])
	//log.Println("小数部分", number[len_number-sca:])
	//log.Println("小数长度：", sca)
	f := number[len_number-sca:]
	if len(f) > 0 {
		return number[:len_number-sca] + "." + f
	}
	return number[:len_number-sca]
}

func MMulInt(left, right, sca, scale int) string {
	n := new(big.Int)
	m := new(big.Int)
	n.SetInt64(int64(left))
	m.SetInt64(int64(right))
	n.Mul(n, m)
	return numberToFloat(n.String(), sca, scale)
}
func MMulStringInt(left string, right, sca, scale int) string {
	n := new(big.Int)
	m := new(big.Int)
	n.SetString(left, 10)
	m.SetInt64(int64(right))
	n.Mul(n, m)
	return numberToFloat(n.String(), sca, scale)
}
func MMulStringInt64(left string, right int64, sca, scale int) string {
	n := new(big.Int)
	m := new(big.Int)
	n.SetString(left, 10)
	m.SetInt64(right)
	n.Mul(n, m)
	return numberToFloat(n.String(), sca, scale)
}
func MMulString(left, right string, sca, scale int) string {
	n := new(big.Int)
	m := new(big.Int)
	n.SetString(left, 10)
	m.SetString(right, 10)
	n.Mul(n, m)
	return numberToFloat(n.String(), sca, scale)
}

func MDivString(left, right string, sca, scale int) string {
	n := new(big.Int)
	m := new(big.Int)
	n.SetString(left, 10)
	m.SetString(right, 10)
	n.Div(n, m)
	return numberToFloat(n.String(), sca, scale)
}

func MAdd(left, right string, sca, scale int) string {
	if left == "0" {
		return right
	}
	if right == "0" {
		return left
	}
	n := new(big.Int)
	m := new(big.Int)
	n.SetString(left, 10)
	m.SetString(right, 10)
	n.Add(n, m)
	return numberToFloat(n.String(), sca, scale)
}
func MSub(left, right string, sca, scale int) string {
	n := new(big.Int)
	m := new(big.Int)
	n.SetString(left, 10)
	m.SetString(right, 10)
	n.Sub(n, m)
	return numberToFloat(n.String(), sca, scale)
}

// left > = right  return true
// left < right false
func MCmp(left, right string) bool {
	n := new(big.Int)
	m := new(big.Int)
	n.SetString(left, 10)
	m.SetString(right, 10)
	//若n<m res=-1 若n==m 则res=0 若n>m 则res=+1
	res := n.Cmp(m)
	if res < 0 {
		return false
	}
	return true

}
