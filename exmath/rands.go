package exmath

import (
	"math/rand"
	"time"
)

//生成随机值
func RandomN(max, min, seed int64) int64 {
	if max <= 1 {
		return max
	}
	Irand := rand.New(rand.NewSource(time.Now().UnixNano() + seed))
	seed = Irand.Int63n(99999999)
	Irand.Seed(time.Now().UnixNano() + seed)
	t := Irand.Int63n(max - min)
	return t + min
}
