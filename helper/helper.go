package helper

import (
	"log"
	"reflect"
	"strconv"
	"strings"
	"time"

	"gitee.com/mowens/moyu/excrypto"
	"gitee.com/mowens/moyu/wordbase"
)

//加密字符串 生成密码
//原字符串sha1 从0截取用户rand_str长度之后的字符，将其拼接于rand_str之后 得字符串1
//字符串1 从末尾截取 rand_num 长度的之前字符，将rand_num 拼接于其后得 字符串2
//字符串2 进行MD5摘要 得密码值
func GetPwd(str, rand_str, rand_num string) string {
	//str0 := excrypto.Sha1(str)
	//fmt.Println(len(str0)) 40
	//str1 := exutf8.RuneSubString(str0, len(rand_str)-1, 40-len(rand_str))
	//fmt.Println(len(str1)) 35
	//str2 := exutf8.RuneSubString(str1, -len(rand_num)-1, 35-len(rand_num))
	return excrypto.Md5(str)
}

func GetToken(str, str2 string, lenstr int) string {
	return excrypto.Md5(str + excrypto.Sha256(wordbase.RandString(lenstr)) + excrypto.Sha1(str) + Int64ToString(time.Now().UnixNano()))
}

func StrToInt(number string) int {
	temp, err := strconv.Atoi(number)
	if err != nil {
		return 0
	}
	return temp
}
func StrToInt32(number string) int32 {
	temp, err := strconv.ParseInt(number, 10, 32)
	if err != nil {
		return 0
	}
	return int32(temp)
}

func StrToInt64(number string) int64 {
	temp, err := strconv.ParseInt(number, 10, 64)
	if err != nil {
		return 0
	}
	return temp
}
func IntToString(i int) string {
	return strconv.Itoa(i)
}
func Int64ToString(i int64) string {
	return strconv.FormatInt(i, 10)
}

//字符串拆分为数组
func StringToArray(str string) []int64 {
	var list [12]int64
	num := 0
	tmp := ""
	for _, v := range str {
		if string(v) != "0" && StrToInt(string(v)) == 0 {
			list[num] = StrToInt64(tmp)
			tmp = ""
			num += 1
		} else {
			tmp += string(v)
		}
	}
	return list[:num]
}

//结构体转map
func StructToMap(obj interface{}) map[string]interface{} {
	obj1 := reflect.TypeOf(obj)
	obj2 := reflect.ValueOf(obj)
	if obj1.Kind() == reflect.Ptr {
		obj1 = obj1.Elem()
	}
	if obj1.Kind() != reflect.Struct {
		log.Println("类型错误，该类型不是结构体“Struct”")
		return nil
	}
	if obj2.Kind() == reflect.Ptr {
		obj2 = obj2.Elem()
	}
	if obj2.Kind() != reflect.Struct {
		log.Println("类型错误，该类型不是结构体“Struct”")
		return nil
	}
	var data = make(map[string]interface{})
	length := obj1.NumField()
	for i := 0; i < length; i++ {
		key := obj1.Field(i).Name
		data[key] = obj2.FieldByName(key).Interface()
	}
	return data
}

//字符串数组去空格
func StrArrayTrim(strs []string) []string {
	for k, v := range strs {
		v = strings.Replace(v, " ", "", -1)
		v = strings.Replace(v, "\r", "", -1)
		v = strings.Replace(v, "\n", "", -1)
		strs[k] = v
	}
	return strs[:]
}

//字符串拆分为string数组
func StrExplode(delimiter, str string) []string {
	strArr := make([]string, 0)
	temp := 0
	for k, v := range str {
		if string(v) == delimiter[:] {
			strArr = append(strArr, str[temp:k])
			temp = k + 1
		}
	}
	strArr = append(strArr, str[temp:])
	return strArr
}

//字符串分割为 int64数组
func StrToIntList(str string, cut string) []int64 {
	list := make([]int64, 0)
	tempindex := 0
	for k, v := range str {
		if string(v) == cut {
			number, err := strconv.ParseInt(str[tempindex:k], 10, 64)
			if err == nil {
				list = append(list, number)
			} else {
				log.Printf("出错了%s \n", err.Error())
				break
			}
			tempindex = k + 1
		}
	}
	number, err := strconv.ParseInt(str[tempindex:], 10, 64)
	if err == nil {
		list = append(list, number)
	} else {
		log.Printf("出错了%s \n", err.Error())
	}
	return list
}
