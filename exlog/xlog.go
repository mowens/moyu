package exlog

import (
	"fmt"
	"log"
	"os"
	"time"
)

var xlog *log.Logger
var fileName, tfname string
var file *os.File

func init() {
	xlog = log.New(os.Stderr, "", log.LstdFlags)
}

// func InitLog() {
// 	xlog = log.New(os.Stderr, "", log.LstdFlags)
// }

//自定义日志文件名称
func SetLogFile(fname string) {
	fileName = fname
}

//创建分割文件
func setLogFiles(name string) (*os.File, error) {
	return os.OpenFile(name, os.O_CREATE|os.O_WRONLY|os.O_APPEND|os.O_SYNC, 0666)
}

//导出日志输出对象
func GetWiter() *os.File {
	setWiter()
	return file
}

//修改日志输出对象
func setWiter() {
	var fname string
	if fileName == "" {
		fname = "xlog"
	} else {
		fname = fileName
	}
	now := time.Now()
	fname += fmt.Sprintf("%d%d%d", now.Year(), now.Month(), now.Day())
	fname += ".log"
	if tfname != fname {
		f, err := setLogFiles(fname)
		file = f
		if err != nil {
			xlog.Printf("创建分割文件失败：%s\n", err.Error())
		}
		xlog.SetOutput(f)
	}
}

//兼容内置log
//格式化输出
func Print(v ...interface{}) {
	setWiter()
	xlog.Print(v...)
}
func Printf(format string, v ...interface{}) {
	setWiter()
	xlog.Printf(format, v...)
}
func Println(v ...interface{}) {
	setWiter()
	xlog.Println(v...)
}

func Fatal(v ...interface{}) {
	setWiter()
	xlog.Fatal(v...)
}

func Fatalf(format string, v ...interface{}) {
	setWiter()
	xlog.Fatalf(format, v...)
}

func Fatalln(v ...interface{}) {
	setWiter()
	xlog.Fatalln(v...)
}

func Panic(v ...interface{}) {
	setWiter()
	xlog.Panic(v...)
}

func Panicf(format string, v ...interface{}) {
	setWiter()
	xlog.Panicf(format, v...)
}

func Panicln(v ...interface{}) {
	setWiter()
	xlog.Panicln(v...)
}
